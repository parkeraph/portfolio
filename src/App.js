import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Navigation from './Components/Navbar'

const App = () => {
  return (
    <div className="App">
      <Navigation/>
        <Router>
          <Route path="/" />
          <Router path="/about/" />
          <Router path="/resume/" />
        </Router>
    </div>
  );
}

export default App;
